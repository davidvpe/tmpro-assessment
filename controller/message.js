//@ts-check

const uuid = require('uuid')
const AWS = require('aws-sdk')
const dynamoDb = new AWS.DynamoDB.DocumentClient()

function getAllMessagesForRecipientId(recipientId) {
    console.log('Getting all messages from a recipient')

    return dynamoDb
        .scan({
            TableName: process.env.MESSAGE_TABLE,
            FilterExpression: 'recipientId = :recipientId',
            ExpressionAttributeValues: {
                ':recipientId': recipientId,
            },
        })
        .promise()
        .then((found) => found.Items)
}

function generateMessage(recipientId, content, method) {
    if (![0, 1].includes(method)) return null
    console.log('Generating message model')

    const timestamp = new Date().getTime()

    return {
        id: uuid.v1(),
        recipientId: recipientId,
        content: content,
        createdAt: timestamp,
        method: method,
    }
}

function saveMessage(message) {
    console.log('Saving message')
    const messageInfo = {
        TableName: process.env.MESSAGE_TABLE,
        Item: message,
    }
    return dynamoDb
        .put(messageInfo)
        .promise()
        .then(() => message)
}

module.exports = {
    getAllMessagesForRecipientId,
    generateMessage,
    saveMessage,
}
