//@ts-check

const uuid = require('uuid')
const AWS = require('aws-sdk')
const dynamoDb = new AWS.DynamoDB.DocumentClient()

async function countRecipientsWith(email, phone) {
    console.log('Checking if similar recipient exists')

    return dynamoDb
        .scan({
            TableName: process.env.RECIPIENT_TABLE,
            FilterExpression: 'email = :email or phone = :phone',
            ExpressionAttributeValues: {
                ':email': email,
                ':phone': phone,
            },
            Select: 'COUNT',
        })
        .promise()
        .then((found) => found.Count)
}

async function saveRecipient(recipient) {
    console.log('Saving recipient')
    const recipientInfo = {
        TableName: process.env.RECIPIENT_TABLE,
        Item: recipient,
    }
    return dynamoDb
        .put(recipientInfo)
        .promise()
        .then(() => recipient)
}

async function getRecipientById(recipientId) {
    console.log('Getting recipient by ID')

    const getQuery = {
        TableName: process.env.RECIPIENT_TABLE,
        Key: {
            id: recipientId,
        },
    }

    return dynamoDb
        .get(getQuery)
        .promise()
        .then((found) => found.Item)
}

function generateRecipient(name, email, phone) {
    console.log('Generating recipient model')

    const timestamp = new Date().getTime()

    return {
        id: uuid.v1(),
        name: name,
        email: email,
        phone: phone,
        createdAt: timestamp,
    }
}

module.exports = {
    countRecipientsWith,
    getRecipientById,
    saveRecipient,
    generateRecipient,
}
