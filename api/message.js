// @ts-check
'use strict'

const uuid = require('uuid')
const AWS = require('aws-sdk')

const dynamoDb = new AWS.DynamoDB.DocumentClient()

const { getRecipientById } = require('../controller/recipient')
const { generateMessage, getAllMessagesForRecipientId, saveMessage } = require('../controller/message')
const { generateError } = require('../utils/error')

module.exports.send = async (event, context, callback) => {
    const { recipientId, message, method } = JSON.parse(event.body)

    if (typeof recipientId !== 'string' || typeof message !== 'string' || typeof method !== 'number') {
        console.error('Validation Failed')
        callback(
            null,
            generateError('Invalid Params', { recipientId: 'string', message: 'string', method: 'number (0 => email, 1 => sms)' })
        )
        return
    }

    //Is this really expected? doesn't seem too lambda-ish
    const recipient = await getRecipientById(recipientId)

    if (!recipient) {
        console.error('Invalid Recipient Id')
        callback(null, generateError('Invalid Recipient Id'))
        return
    }

    const messageCreated = generateMessage(recipientId, message, method)

    if (!messageCreated) {
        console.error('Validation Failed')
        callback(null, generateError('Invalid Params', { method: 'number (0 => email, 1 => sms)' }))
        return
    }

    const savedMessage = await saveMessage(messageCreated)

    let via = ''
    switch (method) {
        case 0:
            console.info('Sending Email....')
            via = 'email'
            break
        case 1:
            console.info('Sending SMS....')
            via = 'sms'
            break
        default:
            break
    }

    const response = {
        statusCode: 200,
        body: JSON.stringify({
            message: 'Message sent successfuly via ' + via,
        }),
    }

    callback(null, response)
}

module.exports.listMessages = async (event, context, callback) => {
    const { recipientId } = event.queryStringParameters
    if (typeof recipientId !== 'string') {
        console.error('Validation Failed')
        callback(null, generateError('Invalid Params', { recipientId: 'string' }))
        return
    }

    //What about infinite records? pagination?
    const messages = await getAllMessagesForRecipientId(recipientId)
    const response = {
        statusCode: 200,
        body: JSON.stringify(messages),
    }

    callback(null, response)
}
