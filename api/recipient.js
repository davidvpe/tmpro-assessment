// @ts-check
'use strict'

const { generateError } = require('../utils/error')
const { countRecipientsWith, saveRecipient, generateRecipient } = require('../controller/recipient')

module.exports.register = async (event, context, callback) => {
    const { name, email, phone } = JSON.parse(event.body)

    if (typeof name !== 'string' || typeof email !== 'string' || typeof phone !== 'string') {
        console.error('Validation Failed')
        callback(null, generateError('Invalid params', { name: 'string', email: 'string', phone: 'string' }))
        return
    }

    const foundRecipients = await countRecipientsWith(email, phone)

    if (foundRecipients > 0) {
        console.error('Existing user with phone or email')
        callback(null, generateError('There is another user already with that phone or email'))
        return
    } else {
        const newRecipient = generateRecipient(name, email, phone)
        const savedRecipient = await saveRecipient(newRecipient)

        const response = {
            statusCode: 201,
            body: JSON.stringify(savedRecipient),
        }
        console.info('User created')
        callback(null, response)
    }
}
