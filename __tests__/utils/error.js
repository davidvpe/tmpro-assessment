const { generateError } = require('../../utils/error')

function parseErrorObject(error) {
    if (!error || !error.body || typeof error.body != 'string') return error
    return { ...error, body: JSON.parse(error.body) }
}

test('check error object is generated correctly', () => {
    const errorGenerated = generateError('ErrorMessage')

    expect(typeof errorGenerated.statusCode).toBe('number')
    expect(typeof errorGenerated.body).toBe('string')
})

test('check error object is generated correctly with error message', () => {
    let errorGenerated = parseErrorObject(generateError('ErrorMessage'))
    expect(errorGenerated).toHaveProperty(['body', 'error'], 'ErrorMessage')
    expect(errorGenerated).toHaveProperty('statusCode', 500)
})

test('check error object is generated correctly with error message and extra info', () => {
    const errorGenerated = parseErrorObject(
        generateError('ErrorMessage', {
            info: {
                name: false,
            },
        })
    )

    expect(errorGenerated).toHaveProperty(['body', 'error'], 'ErrorMessage')
    expect(errorGenerated).toHaveProperty(['body', 'info', 'name'], false)
    expect(errorGenerated).toHaveProperty('statusCode', 500)
})

test('check error object is generated correctly with error message and extra info', () => {
    const errorGenerated = parseErrorObject(generateError('ErrorMessage', {}, 418))

    expect(errorGenerated).toHaveProperty(['body', 'error'], 'ErrorMessage')
    expect(errorGenerated).not.toHaveProperty(['body', 'extra'])
    expect(errorGenerated).toHaveProperty('statusCode', 418)
})
