const { generateMessage, getAllMessagesForRecipientId, saveMessage } = require('../../controller/message')

test('message generated successfuly', () => {
    const testCases = [['1', '2', 0][('1', '2', 1)]]

    testCases.forEach((input) => {
        const message = generateMessage(...input)
        expect(message).toBeDefined()
    })
})

test('message not generated with wrong inputs', () => {
    const message = generateMessage(...['1', '2', 2])
    expect(message).toBeNull()
})

test('message generated with expected properties', () => {
    const message = generateMessage('1', '2', 0)
    expect(message).toHaveProperty('id')
    expect(message).toHaveProperty('recipientId', '1')
    expect(message).toHaveProperty('content', '2')
    expect(message).toHaveProperty('method', 0)
    expect(message).toHaveProperty('createdAt')
})
