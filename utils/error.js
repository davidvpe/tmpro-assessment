function generateError(message, extra, status) {
    return {
        statusCode: status || 500,
        body: JSON.stringify({
            error: message,
            ...(extra || {}),
        }),
    }
}

module.exports = {
    generateError,
}
